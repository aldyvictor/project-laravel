@extends('layouts.master')

@section('title')
  Welcome
@endsection

@section('content')

<section class="content">
	<div class="container-fluid">

		<h1>SELAMAT DATANG {{$firstname}} {{$lastname}}</h1>

		<h2>Terima kasih telah bergabung di SanberBook. Sosial Media Kita Bersama!</h2>

		<a href="{{ route('home') }}">Kembali</a>

	</div>
</section>

@endsection
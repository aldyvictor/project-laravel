@extends('layouts.master')

@section('title')
	All Casts
@endsection

@section('content')

	<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Cast</h1>
          </div><!-- /.col  
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div> /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Detail Cast</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
          	<h3><b>Nama : </b>{{ $cast->nama }}</h3>
            <br>
            <h3><b>Umur : </b>{{ $cast->umur }}</h3>
            <br>
            <h3><b>Bio : </b></h3><br>
            <p>{{ $cast->bio }}</p>
          </div>
          <!-- /.card-body -->
        </div>
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection
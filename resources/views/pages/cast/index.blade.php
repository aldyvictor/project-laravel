@extends('layouts.master')

@section('title')
	All Casts
@endsection

@section('content')

	<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Cast</h1>
          </div><!-- /.col  
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div> /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Tabel Data Cast</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
          	@if(session('success'))
          		<div class="alert alert-success">
          			{{ session('success') }}
          		</div>
          	@endif
          	<a href="{{ route('cast.create') }}" class="btn btn-primary mb-2">+ New Cast</a>
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              	@forelse($casts as $key => $cast)
              		<tr>
              			<td>{{ $key + 1 }}</td>
              			<td>{{ $cast->nama }}</td>
              			<td>{{ $cast->umur }}</td>
              			<td>{{ $cast->bio }}</td>
              			<td align="center">
              				<div>
              					<a href="{{ route('cast.show', $cast->id) }}" class="btn btn-info btn-sm">Show</a>
              					<a href="{{ route('cast.edit', $cast->id) }}" class="btn btn-secondary btn-sm">Edit</a>
              					<form action="{{ route('cast.delete', $cast->id) }}" method="POST" class="d-inline">
              						@csrf
              						@method('DELETE')
              						<input type="submit" value="Hapus" class="btn btn-danger btn-sm">
              					</form>
              				</div>
              			</td>
              		</tr>
              	@empty
              		<tr>
              			<td colspan="4" align="center">Tidak ada Data</td>
              		</tr>
              	@endforelse
              </tbody>
              <tfoot>
              <tr>
               	<th>ID</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection

@push('addon-style')
<link rel="stylesheet" href="{{asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
@endpush

@push('addon-script')
  <script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush
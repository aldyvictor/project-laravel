@extends('layouts.master')

@section('title')
	Edit Cast Baru
@endsection

@section('content')

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Cast Baru</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <div class="card">
          <div class="card-body">

			<form action="{{ route('cast.update', $cast->id) }}" method="POST">
				@csrf
				@method('put')
			    <div class="card-body">
			      <div class="form-group">
			        <label for="nama">Nama</label>
			        <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $cast->nama) }}" placeholder="Masukan Nama" required>
			        @error('nama')
					    <div class="alert alert-danger">{{ $message }}</div>
					@enderror
			      </div>
			      <div class="form-group">
			        <label for="umur">Umur</label>
			        <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', $cast->umur) }}" placeholder="Masukan Umur" required>
			        @error('umur')
					    <div class="alert alert-danger">{{ $message }}</div>
					@enderror
			      </div>
			      <div class="form-group">
                    <label>Bio</label>
                    <textarea class="form-control" rows="3" name="bio" required placeholder="Masukan ..." style="margin-top: 0px; margin-bottom: 0px; height: 105px;" >{{ old('bio', $cast->bio) }}</textarea>
                    @error('bio')
					    <div class="alert alert-danger">{{ $message }}</div>
					@enderror
                  </div>
			    </div>
			    <!-- /.card-body -->

			    <div class="card-footer text-right">
			      <button type="submit" class="btn btn-primary">Submit</button>
			    </div>
			</form>

		</div>
      <!-- /.card-body -->
    </div>
    
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

@endsection
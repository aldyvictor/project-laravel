@extends('layouts.master')

@section('title')
  Buat Akun Baru - SanberCode
@endsection

@section('content')

	<section class="content">
		<div class="container-fluid">
			<form action="{{ route('welcome') }}" method="post" id="inputform">
				@csrf

				<h1>Buat Account Baru!</h1>

				<h3>Sign Up Form</h3>

				<p><label for="firstname">First name :</label></p>

				<input type="text" name="firstname" id="firstname">

				<p><label for="lastname">Last name :</label></p>

				<input type="text" name="lastname" id="lastname">

				<p><label>Gender :</label></p>

				<input type="radio" name="gender" id="male" value="Male">
				<label for="male">Male</label><br>
				<input type="radio" name="gender" id="female" value="Female">
				<label for="female">Female</label><br>
				<input type="radio" name="gender" id="other" value="Other">
				<label for="other">Other</label>

				<p><label for="nationality">Nationality :</label></p>

				<select id="nationality">
					<option value="Indonesian">Indonesian</option>
					<option value="Singapore">Singapore</option>
					<option value="Malaysian">Malaysian</option>
				</select>

				<p><label>Language Spoken :</label></p>

				<input type="checkbox" name="languagespoken" id="indonesia" value="Bahasa Indonesia">
				<label for="indonesia">Bahasa Indonesia</label><br>
				<input type="checkbox" name="languagespoken" id="english" value="English">
				<label for="english">English</label><br>
				<input type="checkbox" name="languagespoken" id="other" value="Other">
				<label for="other">Other</label>

				<p><label for="bio">Bio :</label></p>

				<p><textarea id="bio" rows="7" cols="35" form="inputform"></textarea></p>

				<input type="submit" value="Sign Up">

			</form>
		</div>
	</section>

@endsection
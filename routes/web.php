<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/register', 'AuthController@register')->name('register');

Route::post('/welcome', 'AuthController@welcome')->name('welcome');

Route::get('/table', function () {
	return view('pages.table');
})->name('table');

Route::get('/data-table', function () {
	return view('pages.datatable');
})->name('data-table');

Route::get('/cast', 'CastController@index')->name('cast.index');
Route::get('/cast/create', 'CastController@create')->name('cast.create');
Route::post('/cast', 'CastController@store')->name('cast.store');
Route::get('/cast/{id}', 'CastController@show')->name('cast.show');
Route::get('/cast/{id}/edit', 'CastController@edit')->name('cast.edit');
Route::put('/cast/{id}', 'CastController@update')->name('cast.update');
Route::delete('/post/{id}', 'CastController@destroy')->name('cast.delete');
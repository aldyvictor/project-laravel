<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('cast')->get();

        return view('pages.cast.index', compact('casts'));
    }

    public function create()
    {
        return view('pages.cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required|digits_between:1,3',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect()->route('cast.index')->with('success', 'Cast Berhasil Disimpan!');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('pages.cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('pages.cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required|digits_between:1,3',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
                        ->where('id', $id)
                        ->update([
                            'nama' => $request['nama'],
                            'umur' => $request['umur'],
                            'bio' => $request['bio']
                        ]);

        return redirect()->route('cast.index')->with('success', 'Berhasil di Perbeharui!');
    }

    public function destroy($id)
    {
        $cast = DB::table('cast')->where('id', $id)->delete();

        return redirect()->route('cast.index')->with('success', 'Berhasil di Hapus!');
    }
}

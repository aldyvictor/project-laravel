<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('form');
    }

    public function welcome(Request $req)
    {
        $firstname = $req->firstname;
        $lastname = $req->lastname;
        return view('welcome', compact('firstname', 'lastname'));
    }
}
